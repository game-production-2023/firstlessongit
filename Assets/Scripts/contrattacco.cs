using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class contrattacco : MonoBehaviour
{
  public GameObject ostacoli;
    
    public void OnCollisionEnter2D(Collision2D collision)
    
    {

      if (collision.gameObject.CompareTag("spina"))
      {
        Instantiate(ostacoli, transform.position, transform.rotation);
      }

    }
   
}
