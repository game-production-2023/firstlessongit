using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObject : MonoBehaviour
{
    public GameObject objectPrefab;
    public KeyCode spawnKey = KeyCode.Space;

    private GameObject spawnedObject;

    private void Update()
    {
        if (Input.GetKeyDown(spawnKey))
        {
            StartSpawn();
        }
        else if (Input.GetKeyUp(spawnKey))
        {
            StopSpawn();
        }
    }

    private void StartSpawn()
    {
        if (spawnedObject == null)
        {
            Vector3 spawnPosition = transform.position + transform.right * .8f;

            spawnedObject = Instantiate(objectPrefab, spawnPosition, Quaternion.identity);
        }
    }

    private void StopSpawn()
    {
        if (spawnedObject != null) 
        {
            Destroy(spawnedObject);
            spawnedObject = null;
        }
    }

    private void LateUpdate()
    {
        if (spawnedObject != null)
        {
            spawnedObject.transform.position = transform.position + transform.right * 1f;
        }
    }
}
