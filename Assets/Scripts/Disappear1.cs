using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disappear1 : MonoBehaviour
{
    // Tempo di attesa prima di far scomparire il game object
    public float timeDelay = 2f;

    void Start()
    {
        // Avvia la funzione che farà scomparire il game object dopo un certo tempo
        StartCoroutine(DisappearObject());
    }

    IEnumerator DisappearObject()
    {
        yield return new WaitForSeconds(timeDelay);

        gameObject.SetActive(false);
    }
}
