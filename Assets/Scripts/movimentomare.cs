using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimentomare : MonoBehaviour
{
    [SerializeField] private float jumpForce = 11f;
    [SerializeField] float movementSpeed = 6f;
    private Rigidbody2D mv;
    private bool isGrounded = false;

    private void Start()
    {
        mv = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        float horizontalInput = Input.GetAxisRaw("Horizontal");

        mv.velocity = new Vector2(horizontalInput * movementSpeed, mv.velocity.y);

        if (isGrounded && Input.GetButtonDown("Jump"))
        {
            mv.velocity = new Vector2(mv.velocity.x, jumpForce);
            isGrounded = false;
        }

        else if (Input.GetMouseButtonDown(0))
        {
            isGrounded = false;
        }

        else if (Input.GetMouseButtonUp(0))
        {
            isGrounded = true;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isGrounded = true;
        }
    }
}
